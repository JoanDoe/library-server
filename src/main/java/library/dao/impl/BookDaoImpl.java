package library.dao.impl;

import library.dao.LibraryDao;
import library.dao.util.SearchCriteria;
import library.model.Book;
import library.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.sql.SQLException;
import java.util.*;

/**
 * BookDaoImpl is the class which provides access to database records.
 *
 * @see LibraryDao
 * @see HibernateUtil
 */
public class BookDaoImpl implements LibraryDao<Book> {

	/**
	 * The method used for adding some record in database table.
	 *
	 * @param book the instance of Book class which will be added as record in database
	 * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
	 */
	public void add(Book book) throws SQLException {
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();

			session.beginTransaction();
			session.save(book);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen())) {
				session.close();
			}
		}
	}

	/**
	 * The method used for deleting some record from database.
	 *
	 * @param book the instance of Author class which will be deleted from database.
	 * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
	 */
	public void delete(Book book) throws SQLException {
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();

			session.beginTransaction();
			session.delete(book);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen())) {
				session.close();
			}
		}
	}

	/**
	 * The method used for getting some record from database.
	 *
	 * @param key the primary key of object which will be gotten from database.
	 * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
	 */
	public Book get(int key) throws SQLException {
		Book book = null;
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			book = session.get(Book.class, key);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen())) {
				session.close();
			}
		}
		return book;
	}

	/**
	 * The method used for getting all records from database. Has no input parameters.
	 *
	 * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
	 */
	public List<Book> getAll() throws SQLException {
		List<Book> books = null;
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			books = session.createCriteria(Book.class).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen())) {
				session.close();
			}
		}
		Set<Book> hs = new HashSet<Book>();
		hs.addAll(books);
		books.clear();
		books.addAll(hs);
		return books;
	}


	/**
	 * The method used for update some record in database table.
	 *
	 * @param book the object which will be updated in database
	 */
	public void saveOrUpdate(Book book) throws SQLException {
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();

			session.beginTransaction();
			session.saveOrUpdate(book);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen())) {
				session.close();
			}
		}
	}

	/**
	 * The method used for search data in DB with Criteria API.
	 *
	 * @param searchQuery the instance of SearchQuery class
	 * @return Book collection
	 */
	public List<Book> search(SearchCriteria searchQuery) {
		List<Book> books;

		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria criteria = session.createCriteria(Book.class);

		criteria.createAlias("book", "book");
		criteria.createAlias("book.authors", "author");


		for (HashMap.Entry<String, String> crit : searchQuery.getText().entrySet()) {
			crit.setValue(crit.getValue().replace("*", "%"));
			crit.setValue(crit.getValue().replace("?", "_"));
			criteria.add(Restrictions.like(crit.getKey(), crit.getValue()));
		}

		for (HashMap.Entry<String, Date> crit : searchQuery.getDateFrom().entrySet()) {
			criteria.add(Restrictions.gt(crit.getKey(), crit.getValue()));
		}

		for (HashMap.Entry<String, Date> crit : searchQuery.getDateTo().entrySet()) {
			criteria.add(Restrictions.lt(crit.getKey(), crit.getValue()));
		}

		if (searchQuery.getIsGiven().equals("isGiven")) {
			criteria.add(Restrictions.eq("isGiven", true));
		}
		if (searchQuery.getIsGiven().equals("isNotGiven")) {
			criteria.add(Restrictions.eq("isGiven", false));
		}

		books = criteria.list();

		if ((session != null) && (session.isOpen())) {
			session.close();
		}
		Set<Book> hs = new HashSet<Book>();
		hs.addAll(books);
		books.clear();
		books.addAll(hs);
		return books;
	}

}
