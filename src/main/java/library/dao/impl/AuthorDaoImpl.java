package library.dao.impl;

import library.dao.LibraryDao;
import library.model.Author;
import library.util.HibernateUtil;
import org.hibernate.Session;

import java.sql.SQLException;
import java.util.List;

/**
 * AuthorDaoImpl is the class which provides access to database records.
 *
 * @see LibraryDao
 * @see HibernateUtil
 */
public class AuthorDaoImpl implements LibraryDao<Author> {

	/**
	 * The method used for adding some record in database table.
	 *
	 * @param author the instance of Author class which will be added as record in database
	 * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
	 */
	public void add(Author author) throws SQLException {
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(author);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen())) {
				session.close();
			}
		}
	}

	/**
	 * The method used for deleting some record from database.
	 *
	 * @param author the instance of Author class which will be deleted from database
	 * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
	 */
	public void delete(Author author) throws SQLException {
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();

			session.beginTransaction();
			session.delete(author);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen())) {
				session.close();
			}
		}
	}

	/**
	 * The method used for getting some record from database.
	 *
	 * @param key the primary key of object which will be gotten from database.
	 * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
	 */
	public Author get(int key) throws SQLException {
		Author author = null;
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			author = session.get(Author.class, key);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen())) {
				session.close();
			}
		}
		return author;
	}

	/**
	 * The method used for getting all records from database. Has no input parameters.
	 *
	 * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
	 */
	public List<Author> getAll() throws SQLException {
		List<Author> authors = null;
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			authors = session.createCriteria(Author.class).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen())) {
				session.close();
			}
		}
		return authors;
	}


	/**
	 * The method used for update some record in database table.
	 *
	 * @param author the object which will be updated in database
	 */
	public void saveOrUpdate(Author author) throws SQLException {
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();

			session.beginTransaction();
			session.saveOrUpdate(author);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen())) {
				session.close();
			}
		}
	}
}
