package library.dao.impl;

import library.dao.LibraryDao;
import library.model.AbstractBook;
import library.util.HibernateUtil;
import org.hibernate.Session;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * AbstractBookDaoImpl is the class which provides access to database records.
 *
 * @see LibraryDao
 * @see HibernateUtil
 */
public class AbstractBookDaoImpl implements LibraryDao<AbstractBook> {

	/**
	 * The method used for adding some record in database table.
	 *
	 * @param abstractBook the instance of AbstractBook class which will be added as record in database
	 * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
	 */
	public void add(AbstractBook abstractBook) throws SQLException {
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();

			session.beginTransaction();
			session.save(abstractBook);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen())) {
				session.close();
			}
		}
	}

	/**
	 * The method used for deleting some record from database table.
	 *
	 * @param abstractBook the instance of AbstractBook class which will be deleted from database
	 * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other errors.
	 */
	public void delete(AbstractBook abstractBook) throws SQLException {
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();

			session.beginTransaction();
			session.delete(abstractBook);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen())) {
				session.close();
			}
		}
	}

	/**
	 * The method used for getting some record from database table.
	 *
	 * @param key the primary key of object which will be gotten from database
	 * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other errors.
	 */
	public AbstractBook get(int key) throws SQLException {
		AbstractBook abstractBook = null;
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			abstractBook = session.get(AbstractBook.class, key);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen())) {
				session.close();
			}
		}
		return abstractBook;
	}

	/**
	 * The method used for getting all objects from database. Has no input parameters.
	 *
	 * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other errors.
	 */
	public List<AbstractBook> getAll() throws SQLException {
		List<AbstractBook> abstractBooks = null;
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			abstractBooks = session.createCriteria(AbstractBook.class).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen())) {
				session.close();
			}
		}
		Set<AbstractBook> hs = new HashSet<AbstractBook>();
		hs.addAll(abstractBooks);
		abstractBooks.clear();
		abstractBooks.addAll(hs);
		return abstractBooks;
	}


	/**
	 * The method used for update some record in database table.
	 *
	 * @param abstractBook the object which will be updated in database
	 */
	public void saveOrUpdate(AbstractBook abstractBook) throws SQLException {
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();

			session.beginTransaction();
			session.saveOrUpdate(abstractBook);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen())) {
				session.close();
			}
		}
	}
}
