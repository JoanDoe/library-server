package library.dao.util;

import library.dao.impl.AbstractBookDaoImpl;
import library.dao.impl.AuthorDaoImpl;
import library.dao.impl.BookDaoImpl;

/**
 * Factory is the class which provides getting classes implements data access object.
 *
 * @see AuthorDaoImpl
 * @see AbstractBookDaoImpl
 * @see BookDaoImpl
 */
public class Factory {

	/**
	 * A static variable representing instance of this (Factory) class.
	 */
	private static Factory instance = new Factory();

	/**
	 * A variable representing instance of AuthorDaoImpl class.
	 */
	private AuthorDaoImpl authorDaoImpl;

	/**
	 * A variable representing instance of AbstractBookDaoImpl class.
	 */
	private AbstractBookDaoImpl abstractBookDaoImpl;

	/**
	 * A variable representing instance of BookDaoImpl class.
	 */
	private BookDaoImpl bookDaoImpl;

	/**
	 * A default constructor of this(Factory) class.
	 */
	private Factory() {
	}

	/**
	 * The method used for getting instance of this (Factory) class. Has no input parameters.
	 */
	public static Factory getInstance() {
		return Factory.instance;
	}

	/**
	 * The method used for getting instance of AuthorDaoImpl class. Has no input parameters.
	 */
	public AuthorDaoImpl getAuthorDao() {
		if (authorDaoImpl == null) {
			authorDaoImpl = new AuthorDaoImpl();
		}
		return authorDaoImpl;
	}

	/**
	 * The method used for getting instance of AbstractBookDaoImpl class. Has no input parameters.
	 */
	public AbstractBookDaoImpl getAbstractBookDao() {
		if (abstractBookDaoImpl == null) {
			abstractBookDaoImpl = new AbstractBookDaoImpl();
		}
		return abstractBookDaoImpl;
	}

	/**
	 * The method used for getting instance of BookDaoImpl class. Has no input parameters.
	 */
	public BookDaoImpl getBookDao() {
		if (bookDaoImpl == null) {
			bookDaoImpl = new BookDaoImpl();
		}
		return bookDaoImpl;
	}
}
