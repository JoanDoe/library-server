package library;

import library.controller.socket.SocketController;
import library.task.Task;

import java.io.*;

import java.net.*;

class ServeOneJabber extends Thread {
	private Socket socket;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;

	public ServeOneJabber(Socket s) throws IOException {
		socket = s;
		BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
		ois = new ObjectInputStream(bis);
		BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
		oos = new ObjectOutputStream(bos);
		start();
	}

	public void run() {
		try {
			Task testTask = null;
			try {
				testTask = (Task) ois.readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			SocketController sc = new SocketController(testTask);
			oos.writeObject(sc.getResponse());
			oos.flush();
			System.out.println("closing...");
		} catch (IOException e) {
			System.err.println("IO Exception");
		} finally {
			try {
				oos.close();
				ois.close();
				socket.close();
			} catch (IOException e) {
				System.err.println("Socket not closed");
			}
		}
	}
}

public class Server {
	static final int PORT = 8081;

	public static void main(String[] args) throws IOException {
		ServerSocket s = new ServerSocket(PORT);
		System.out.println("Server Started");
		try {
			while (true) {
				Socket socket = s.accept();
				try {
					new ServeOneJabber(socket);
				} catch (IOException e) {
					socket.close();
				}
			}
		} finally {
			s.close();
		}
	}
}