package library.model;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "abstractBook")
@Entity
@Table(name = "abstract_books")
/**
 *  AbstractBook is the class representing specific literary work.
 *  @see Author
 */
public class AbstractBook implements Serializable {

	@Id
	@Column(name = "book_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@XmlAttribute(name = "id")
	/**
	 * A variable representing unique identifier of record.
	 */
	private int id;

	@XmlElementWrapper(name = "authors")
	@XmlElement(name = "author")
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "author_abstractBook", joinColumns = @JoinColumn(name = "book_id"), inverseJoinColumns = @JoinColumn(name = "author_id"))
	/**
	 * A variable representing author of work.
	 */
	private Set<Author> authors;


	@Column(name = "title")
	/**
	 * A variable representing title of work.
	 */
	private String title;

	@Column(name = "publication_date")
	/**
	 * A variable representing the date of publication.
	 */
	private Date publicationDate;

	@Column(name = "genre")
	/**
	 * A variable representing genre of work.
	 */
	private String genre;

	/**
	 * The method used for getting value of author field.
	 *
	 * @return the instance of Author class
	 */
	public Set<Author> getAuthors() {
		return authors;
	}

	/**
	 * The method used for setting value of author field.
	 *
	 * @param authors the value of author field which will be set.
	 */
	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	/**
	 * The method used for getting value of title field.
	 *
	 * @return the string contains title of book
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * The method used for setting value of title field.
	 *
	 * @param title the value of title field which will be set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * The method used for getting value of publicationDate field.
	 *
	 * @return the instance of Date class contains publication date
	 */
	public Date getPublicationDate() {
		return publicationDate;
	}

	/**
	 * The method used for setting value of publicationDate field.
	 *
	 * @param publicationDate the value of publicationDate field which will be set.
	 */
	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	/**
	 * The method used for getting value of id field.
	 *
	 * @return the int value contains id of book
	 */
	public int getId() {
		return id;
	}

	/**
	 * The method used for setting value of id field.
	 *
	 * @param id the value of id field which will be set.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * The method used for getting value of genre field.
	 *
	 * @return the string contains genre of book
	 */
	public String getGenre() {
		return genre;
	}

	/**
	 * The method used for setting value of genre field.
	 *
	 * @param genre the value of genre field which will be set.
	 */
	public void setGenre(String genre) {
		this.genre = genre;
	}

	/**
	 * The method used for equals instance of this class object and other object.
	 *
	 * @param object the object for comparing.
	 * @return boolean value
	 */
	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}

		if (object == this) {
			return true;
		}

		if (object instanceof AbstractBook) {
			Boolean classAndTittle = false;
			AbstractBook other = (AbstractBook) object;
			if (((AbstractBook) object).getClass().getSimpleName().equals(this.getClass().getSimpleName())
					& (other.title.toLowerCase().equals(this.title.toLowerCase()))) {
				classAndTittle = true;
			}

			Boolean authorsEquals = false;

			if (other.getAuthors().size() != this.getAuthors().size()) {
				return false;
			}

			int equalsCount = 0;

			for (Author otherAuthor : other.getAuthors()) {
				for (Author thisAuthor : this.getAuthors()) {
					if (otherAuthor.equals(thisAuthor)) {
						equalsCount++;
						break;
					}
				}
			}
			if (equalsCount == this.getAuthors().size()) {
				authorsEquals = true;
			}

			if (classAndTittle & authorsEquals) {
				return true;
			}
		}
		return false;
	}
}