package library.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "books")
/**
 *  Library is the class representing collection of material books.
 *  @see Book
 *  @see List
 */
public class Library implements Serializable {

	@XmlElement(name = "book")
	/**
	 * A variable representing collection of material books.
	 */
	private List<Book> books;

	/**
	 * The method used for getting value of books field.
	 *
	 * @return the collection of material books
	 */
	public List<Book> getBooks() {
		return books;
	}

	/**
	 * The method used for setting value of books field.
	 *
	 * @param books the value of books field which will be set.
	 */
	public void setBooks(List<Book> books) {
		this.books = books;
	}
}
