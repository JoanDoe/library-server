package library.controller;

import library.dao.impl.BookDaoImpl;
import library.model.Library;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * BookExporter is the class implements export of Book collection.
 *
 * @see Marshaller
 * @see JAXBException
 * @see OutputStream
 * @see Library
 */
public class BookExporter {

	/**
	 * A variable representing path to file in which data will be exported.
	 */
	private String fileName;

	/**
	 * It is the constructor which initialize fileName field.
	 */
	public BookExporter() {

	}

	/**
	 * The method used for marshalling data.
	 *
	 * @param library the instance of Library class which will be exported in xml file.
	 * @throws JAXBException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void marshal(Library library) {
		Calendar calendar = Calendar.getInstance();
		Timestamp timestamp = new Timestamp(calendar.getTime().getTime());
		fileName=new String(new String("file" + timestamp.getTime() +".xml"));

		try {
			JAXBContext jc = JAXBContext.newInstance(Library.class);
			Marshaller m = jc.createMarshaller();
			OutputStream os = new FileOutputStream(fileName);
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(library, os);
			os.close();
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * The method used for export Book collection from database table.
	 */
	public String exportBooks(){

		Library library = new Library();
		BookDaoImpl bookDao = new BookDaoImpl();
		try {
			library.setBooks(bookDao.getAll());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		marshal(library);


		String fileContext = readFile(fileName);

		File file = new File(fileName);
		if(file.delete()){
			System.out.println(file.getName() + " is deleted!");
		}else{
			System.out.println("Delete operation is failed.");
		}

		return fileContext;
	}

	private String readFile(String filePath ) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader( new FileReader(filePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String         line;
		StringBuilder  stringBuilder = new StringBuilder();
		String         ls = System.getProperty("line.separator");

		try {
			while( ( line = reader.readLine() ) != null ) {
                stringBuilder.append( line );
                stringBuilder.append( ls );
            }
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return stringBuilder.toString();
	}
}
