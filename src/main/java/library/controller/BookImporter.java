package library.controller;

import library.dao.impl.AbstractBookDaoImpl;
import library.dao.impl.AuthorDaoImpl;
import library.dao.impl.BookDaoImpl;
import library.dao.util.Factory;
import library.model.AbstractBook;
import library.model.Author;
import library.model.Book;
import library.model.Library;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.Date.*;

/**
 * BookImporter is the class implements import of Book collection.
 *
 * @see Unmarshaller
 * @see JAXBException
 * @see InputStream
 * @see Library
 * @see Factory
 */
public class BookImporter {

	/**
	 * A variable representing path to file from which data will be imported.
	 */
	private String fileName;

	/**
	 * It is the constructor which initialize file field.
	 *
	 * @param file the value of file field.
	 */
	public BookImporter(String file) {
		PrintWriter writer = null;

		Calendar calendar = Calendar.getInstance();
		Timestamp timestamp = new Timestamp(calendar.getTime().getTime());
		fileName=new String(new String("file" + timestamp.getTime() +".xml"));

		try {
			writer = new PrintWriter(fileName, "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		writer.write(file);
		writer.close();
	}

	/**
	 * The method used for unmarshalling data.
	 *
	 * @throws JAXBException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private List<Book> unmarshal() {
		Library library = null;

		try {
			JAXBContext jc = JAXBContext.newInstance(Library.class);
			InputStream is = new FileInputStream(fileName);
			Unmarshaller um = jc.createUnmarshaller();
			library = (Library) um.unmarshal(is);
			is.close();
			File file = new File(fileName);
			file.delete();
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return library.getBooks();
	}


	/**
	 * The method used for import object in database table.
	 *
	 * @return the collection of books
	 * @throws SQLException
	 */
	public void importBooks() throws SQLException {
		List<Book> books = unmarshal();
		Factory factory = Factory.getInstance();
		AuthorDaoImpl authorDao = factory.getAuthorDao();
		AbstractBookDaoImpl aBookDao = factory.getAbstractBookDao();
		BookDaoImpl bookDao = factory.getBookDao();
		boolean authorContains = false;
		boolean aBookContains = false;

		for (Book book : books) {
			AbstractBook aBook = book.getBook();
			Set<Author> authors = aBook.getAuthors();
			Set<Author> authorsToAddNew = new HashSet<Author>();
			Set<Author> authorsToAddExisting = new HashSet<Author>();

			book.setId(0);
			for (Author author : authors) {
				authorContains = false;
				author.setId(0);
				for (Author existingAuthor : authorDao.getAll()) {
					if (author.equals(existingAuthor)) {
						//aBook.setAuthor(existingAuthor);
						//authorsToAddNew.remove(author);
						authorsToAddExisting.add(existingAuthor);
						authorContains = true;
						break;
					}
				}


				if (!authorContains) {
					authorsToAddNew.add(author);
				}
			}

			authorsToAddExisting.addAll(authorsToAddNew);
			aBook.setAuthors(authorsToAddExisting);

			aBook.setId(0);
			for (AbstractBook existingABook : aBookDao.getAll()) {
				if (aBook.equals(existingABook)) {
					book.setBook(existingABook);
					aBookContains = true;
					break;
				}
			}

			if (!aBookContains) {
				aBookDao.add(aBook);
				book.setBook(aBook);
			}

			bookDao.add(book);
			aBookContains = false;
		}
	}

}
