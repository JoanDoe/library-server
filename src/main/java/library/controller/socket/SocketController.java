package library.controller.socket;

import library.controller.BookExporter;
import library.controller.BookImporter;
import library.dao.impl.AbstractBookDaoImpl;
import library.dao.impl.AuthorDaoImpl;
import library.dao.impl.BookDaoImpl;
import library.dao.util.SearchCriteria;
import library.model.AbstractBook;
import library.model.Author;
import library.model.Book;
import library.task.Task;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by �� on 12/13/2015.
 */
public class SocketController implements Serializable {
	private Task task;
	private Object response;

	public SocketController(Task task) {
		this.setTask(task);
		createRequest();
	}

	private void createRequest() {
		if (getTask().getMethodName().equals("import")) {
			importBooks();
		}
		if (getTask().getMethodName().equals("export")) {
			response = exportBooks();
		}

		if (getTask().getMethodName().equals("search")) {
			this.setResponse(this.search());
		}
		if (getTask().getMethodName().equals("get")) {

			if (task.getClassName().equals("Author")) {
				this.setResponse(this.getAuthor());
			}
			if (task.getClassName().equals("AbstractBook")) {
				this.setResponse(this.getAbook());
			}
			if (task.getClassName().equals("Book")) {
				this.setResponse(this.getBook());
			}
		}

		if (getTask().getMethodName().equals("add")) {

			if (task.getClassName().equals("Author")) {
				this.setResponse(this.addAuthor());
			}
			if (task.getClassName().equals("AbstractBook")) {
				this.setResponse(this.addAbook());
			}
			if (task.getClassName().equals("Book")) {
				this.setResponse(this.addBook());
			}
		}

		if (getTask().getMethodName().equals("delete")) {

			if (task.getClassName().equals("Author")) {
				this.setResponse(this.deleteAuthor());
			}
			if (task.getClassName().equals("AbstractBook")) {
				this.setResponse(this.deleteAbook());
			}
			if (task.getClassName().equals("Book")) {
				this.setResponse(this.deleteBook());
			}
		}

		if (getTask().getMethodName().equals("getAll")) {

			if (task.getClassName().equals("Author")) {
				this.setResponse(this.getAuthors());
			}
			if (task.getClassName().equals("AbstractBook")) {
				this.setResponse(this.getAbooks());
			}
			if (task.getClassName().equals("Book")) {
				this.setResponse(this.getBooks());
			}
		}

		if (getTask().getMethodName().equals("saveOrUpdate")) {

			if (task.getClassName().equals("Author")) {
				this.setResponse(this.saveOrUpdateAuthor());
			}
			if (task.getClassName().equals("AbstractBook")) {
				this.setResponse(this.saveOrUpdateAbook());
			}
			if (task.getClassName().equals("Book")) {
				this.setResponse(this.saveOrUpdateBook());
			}
		}

	}

	private String exportBooks() {
		//String path = (String) task.getParam();
		BookExporter bookExporter = new BookExporter();
		return bookExporter.exportBooks();
	}


	private void importBooks() {
		String path = (String) task.getParam();
		BookImporter bookImporter = new BookImporter(path);
		try {
			bookImporter.importBooks();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private List<Book> search() {
		SearchCriteria searchCriteria = (SearchCriteria) task.getParam();
		BookDaoImpl bookDao = new BookDaoImpl();
		return bookDao.search(searchCriteria);
	}

	private Author getAuthor() {
		int i = (Integer) task.getParam();
		AuthorDaoImpl authorDao = new AuthorDaoImpl();
		try {
			return authorDao.get(i);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Boolean addAuthor() {
		Author author = (Author) task.getParam();
		AuthorDaoImpl authorDao = new AuthorDaoImpl();
		try {
			authorDao.add(author);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	private Boolean saveOrUpdateAuthor() {
		Author author = (Author) task.getParam();
		AuthorDaoImpl authorDao = new AuthorDaoImpl();
		try {
			authorDao.saveOrUpdate(author);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	private Boolean deleteAuthor() {
		Author author = (Author) task.getParam();
		AuthorDaoImpl authorDao = new AuthorDaoImpl();
		try {
			authorDao.delete(author);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	private List<Author> getAuthors() {
		AuthorDaoImpl authorDao = new AuthorDaoImpl();
		try {
			return authorDao.getAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private AbstractBook getAbook() {
		int i = (Integer) task.getParam();
		AbstractBookDaoImpl abstractBookDao = new AbstractBookDaoImpl();
		try {
			return abstractBookDao.get(i);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Boolean addAbook() {
		AbstractBook abstractBook = (AbstractBook) task.getParam();
		AbstractBookDaoImpl abstractBookDao = new AbstractBookDaoImpl();
		try {
			abstractBookDao.add(abstractBook);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	private Boolean saveOrUpdateAbook() {
		AbstractBook abstractBook = (AbstractBook) task.getParam();
		AbstractBookDaoImpl abstractBookDao = new AbstractBookDaoImpl();
		try {
			abstractBookDao.saveOrUpdate(abstractBook);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	private Boolean deleteAbook() {
		AbstractBook abstractBook = (AbstractBook) task.getParam();
		AbstractBookDaoImpl abstractBookDao = new AbstractBookDaoImpl();
		try {
			abstractBookDao.delete(abstractBook);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	private List<AbstractBook> getAbooks() {
		AbstractBookDaoImpl abstractBookDao = new AbstractBookDaoImpl();
		try {
			return abstractBookDao.getAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Book getBook() {
		int i = (Integer) task.getParam();
		BookDaoImpl bookDao = new BookDaoImpl();
		try {
			return bookDao.get(i);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


	private Boolean addBook() {
		Book book = (Book) task.getParam();
		BookDaoImpl bookDao = new BookDaoImpl();
		try {
			bookDao.add(book);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	private Boolean saveOrUpdateBook() {
		Book book = (Book) task.getParam();
		BookDaoImpl bookDao = new BookDaoImpl();
		try {
			bookDao.saveOrUpdate(book);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	private Boolean deleteBook() {
		Book book = (Book) task.getParam();
		BookDaoImpl bookDao = new BookDaoImpl();
		try {
			bookDao.delete(book);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	private List<Book> getBooks() {
		BookDaoImpl bookDao = new BookDaoImpl();
		try {
			return bookDao.getAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}
}
