package library.util;

import library.model.AbstractBook;
import library.model.Author;
import library.model.Book;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	/**
	 * A variable representing instance of SessionFactory class.
	 */
	private static SessionFactory sessionFactory;

	static {
		try {
			Configuration configuration = new Configuration();
			configuration.configure("hibernate.cfg.xml");
			configuration.addAnnotatedClass(Author.class);
			configuration.addAnnotatedClass(AbstractBook.class);
			configuration.addAnnotatedClass(Book.class);
			StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
			sessionFactory = configuration.buildSessionFactory(ssrb.build());
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	/**
	 * The method used for getting sessionFactory value.
	 */
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
